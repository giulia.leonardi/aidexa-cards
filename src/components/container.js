import React, { Component } from 'react';
import Grid from '@material-ui/core/Grid';
import Card from './card'
import selected from './../img/selected.png'


export default class Container extends Component {
    constructor(props) {
        super(props);
        this.state = {
            products: [],
            categories: [],
            filteredCategories: [],
        }
    }

    componentDidMount() {
        this.getProducts()
    }

    getProducts() {
        const requestOptions = { method: 'GET' };
        return fetch(`https://gorest.co.in/public-api/products`, requestOptions)
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({ products: result.data })
                    this.retrieveCategories(result.data)
                },
                (error) => {
                    console.log(error)
                }
            )
    }

    retrieveCategories(products) {
        let categories = [];
        for (let prod of products) {
            for (let prodCategory of prod.categories) {
                if (!categories.find(localCategory => localCategory.id === prodCategory.id)) {
                    categories.push({ id: prodCategory.id, name: prodCategory.name })
                }
            }
        }
        this.setState({ categories })
    }

    addCategory(id) {
        this.setState({ filteredCategories: [...this.state.filteredCategories, id] })
    }

    removeCategory(id) {
        this.setState({ filteredCategories: this.state.filteredCategories.filter(category => category !== id) })
    }

    render() {
        const { categories, filteredCategories, products } = this.state;

        return (
            <div style={styles.container}>
                <div style={styles.header}>
                    <p style={styles.title}>Filter results</p>
                    <Grid container>
                        {categories.map((category, key) => {
                            return filteredCategories.find(cat => cat === category.id) ?
                                <button key={key} style={styles.selectedCategory} onClick={() => this.removeCategory(category.id)}>
                                    <img src={selected} alt="selected category" style={styles.selected} />
                                    <p style={styles.name}>{category.name}</p></button> :
                                <button key={key} style={styles.category} onClick={() => this.addCategory(category.id)}>
                                    <p style={styles.name}>{category.name}</p></button>
                        })}
                    </Grid>
                </div>
                <Grid container>
                    {products.filter(product => filteredCategories.length < 1 ? true : product.categories.some(category => filteredCategories.includes(category.id))).map((product, key) => {
                        return <Grid key={key} item xs={12} sm={6} md={4} lg={3}><Card product={product} /></Grid>
                    })}
                </Grid>
            </div>
        )
    }
}

const styles = {
    container: {
        height: '100vh',
        backgroundColor: '#dbdcff',
        overflowX: 'scroll'
    },
    selectedCategory: {
        margin: 5,
        borderRadius: 20,
        border: '1px solid white',
        backgroundColor: '#8083e5',
        color: 'white',
        padding: 15,
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        outline: 'none',
    },
    category: {
        margin: 5,
        borderRadius: 20,
        border: 'none',
        backgroundColor: 'white',
        padding: 15,
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        outline: 'none'
    },
    selected: {
        height: 10,
        objectFit: 'contain',
        marginRight: 10
    },
    name: {
        margin: 0
    },
    header: {
        backgroundColor: '#a8aaff',
        padding: 40
    },
    title: {
        textAlign: 'left',
        fontWeight: '700',
        color: 'white',
        fontSize: 25,
        marginTop: 5,
        marginLeft: 10
    }
}