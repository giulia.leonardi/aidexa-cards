import React, { useState } from 'react';
import arrow from './../img/arrow.png'

const Card = ({ product }) => {
    const [openInfo, setOpenInfo] = useState(false);

    return (
        <div style={styles.container}>
            <img style={styles.header} src={product.image} alt="product" />
            <div style={styles.fixedRow}>
                <p style={styles.name}>{product.name}</p>
                <button style={styles.button} onClick={() => setOpenInfo(!openInfo)}><img src={arrow} alt="open info" style={openInfo ? styles.arrowUp : styles.arrowDown} /></button>
            </div>
            {openInfo && <div style={styles.info}>{product.description}</div>}
        </div>
    );
}

export default Card;

const styles = {
    container: {
        display: 'flex',
        flexDirection: 'column',
        backgroundColor: 'white',
        borderRadius: 10,
        margin: 20,
        boxShadow: "2px 2px 2px #8f92ff"
    },
    header: {
        height: 200,
        width: '100%',
        objectFit: 'cover',
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10
    },
    fixedRow: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        margin: 20
    },
    name: {
        fontSize: 22,
        fontWeight: '700',
        color: '#454545'
    },
    info: {
        margin: 20,
        marginTop: 0,
        textAlign: 'left',
        color: '#454545'
    },
    button: {
        outline: 'none',
        backgroundColor: 'transparent',
        border: 'none'
    },
    arrowDown: {
        height: 10,
        objectFit: 'contain',
    },
    arrowUp: {
        height: 10,
        objectFit: 'contain',
        transform: 'rotate(180deg)'
    }
}